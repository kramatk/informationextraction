# README #

This is Information Extraction homework in NLP class , Kasetsart University

### Install ###
* sudo pip install nltk [tools for tokenize text]

* git clone https://kramatk@bitbucket.org/kramatk/informationextraction.git [clone project]

* python regx.py <input file> [Run it!! ex python regx.py corpus/sport1.txt ]

### Method ###
* read file country.txt (and nationality.txt) and list all country name
* tokenize text and then tag word that is country name by <CT>....</CT>
* read rule.txt and change it to REGEX
* create SLOT




### Thank ###
* NLTK , Natural Language Toolkit
* [Readmore](http://www.nltk.org/)
# -*- coding: utf-8 -*-
import re
import sys
from nltk.tokenize.punkt import PunktWordTokenizer

class Slot:
	def __init__(self,CT_1="",CT_2="",score=""):
		self.country_1 = CT_1
		self.country_2 = CT_2
		sp = score.split("-")
		if sp[0]==sp[1]:
			self.winner = "drew"
		else:
			self.winner = CT_1
		self.score = score

	def __str__(self):
		s="-----------------------------\n"
		s+="country_1 :" + self.country_1+"\n"
		s+="country_2 :" + self.country_2+"\n"
		s+="winner :" + self.winner+"\n"
		s+="score :" + self.score+"\n"
		s+="-----------------------------\n"
		return s

		
country= []
fcountry = open("country.txt",'r')
for cc in fcountry.readlines():
	cc = cc.lower().strip().split("|")
	country.extend(cc[1].split(" "))

f = open(sys.argv[1])
txt = f.read().lower().replace(".",'')
token = PunktWordTokenizer().tokenize(txt)

s=""
for i, t in enumerate(token):

	if token[i] in country:
		
		flag = token[i]
		if (i==0) or (i>=1 and (token[i-1] not in country)):
			k=1
			while i+k < len(token) and token[i+k] in country:
				flag += " "+token[i+k]
				k+=1
		elif i>=1:
			continue
			
		s+="<CT>"+flag+"</CT> "
	else:	
		s+=t+" "

print "Preprocessing"
print s


def pattern(type_,attr):
	if type_ == "CT":
		return r'<CT>(?P<'+attr+r'>[\w\s]+?)</CT>'
	elif type_ =="SCORE":
		return r'(?P<'+attr+r'>\d+-\d+)'
	elif type_ == "WORD":
		return r'(\s+[^\s<]*\s*){0,'+str(attr)+'}'

rules = []
frule = open("rule.txt",'r')
for r in frule.readlines():
	st = r.strip().split(" ")
	rule = ""
	for part in st:
		p = part.split(":")
		if p[0]!="S":
			rule += pattern(p[0],p[1])
		else:
			rule += p[1]
	rules.append(rule)

slots = []

print "\nSlot"
for r in rules:
	#print r
	p = re.compile(r)
	for m in p.finditer(s):
		out = m.groupdict()
		country_1 =""
		country_2 =""
		score =""

		if out.has_key("country_1"):
			country_1=out["country_1"]
		if out.has_key("country_2"):
			country_2=out["country_2"]
		if out.has_key("score"):
			score=out["score"]
		slot = Slot(country_1,country_2,score)
		slots.append(slot)
		print slot


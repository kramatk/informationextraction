Thai footballers sweep China aside

INCHEON � The Thai national team cruised into the quarter finals of the Asian Games football after defeating China 2-0 on Thursday.

Two goals from substitute Adisak Kraisorn in the second half earned the team a place in the last 16 of the men's soccer contest in Incheon. Adisak was sent in for Chananan Pombupha who suffered a knee injury and will be out of action for five months. The Thais will meet Jordan on Sunday. Jordan defeated Kyrgyzstan 2-0 in their latest match. After the win, the Football Association of Thailand awarded 500,000 baht to the team plus 200,000 baht for each goal they scored.


Asian Games football: Singapore beat Palestine 2-1 but fail to make it to next round 

Published on Sep 21, 2014 6:06 PM

SINGAPORE'S Under-23 team are out of the Asian Games men's football competition despite beating Palestine 2-1 in their final Group C match.

Two superbly-taken free kicks by Shahfiq Ghani in the second and 18th minutes, both swung in exquisitely with his left foot, had given the Young Lions an early lead which they never relinquished despite Ahmed Wridat Suad pulling a goal back for Palestine in the 82nd minute.

But despite ending their group matches with a win, giving Singapore four points, Tajikistan's 1-0 victory over Oman means they, along with group winners Palestine, proceed to the next round.

Singapore end the tournament third in the group, three points more than Oman.